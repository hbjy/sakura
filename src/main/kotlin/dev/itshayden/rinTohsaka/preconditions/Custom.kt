package dev.itshayden.rinTohsaka.preconditions

import dev.itshayden.rinTohsaka.services.GuildService
import me.aberrantfox.kjdautils.api.dsl.Precondition
import me.aberrantfox.kjdautils.api.dsl.precondition
import me.aberrantfox.kjdautils.internal.command.Fail
import me.aberrantfox.kjdautils.internal.command.Pass

@Precondition
fun customCommandPrecondition(guildService: GuildService) = precondition {
    val commandName = it.commandStruct.commandName
    val command = it.container[commandName]

    if (command != null)
        return@precondition Pass

    val macro =
        guildService.getGuildCommands(it.guild!!).filter {
                it.name.toLowerCase() == commandName.toLowerCase()
            }.shuffled().firstOrNull() ?: return@precondition Fail("No such macro in this guild!")

    it.respond(macro.response)

    Pass
}

@Precondition
fun customCommandPreconditionTwo() = precondition {
    it.container[it.commandStruct.commandName] ?: return@precondition Fail()
    Pass
}