package dev.itshayden.rinTohsaka.preconditions

import dev.itshayden.rinTohsaka.Rin
import me.aberrantfox.kjdautils.api.dsl.Precondition
import me.aberrantfox.kjdautils.api.dsl.command.Command
import me.aberrantfox.kjdautils.api.dsl.precondition
import me.aberrantfox.kjdautils.extensions.jda.toMember
import me.aberrantfox.kjdautils.internal.command.Fail
import me.aberrantfox.kjdautils.internal.command.Pass
import net.dv8tion.jda.api.Permission

private val permissionList: MutableMap<Command, List<Permission>> = mutableMapOf()
private val ownerOnlyList: MutableMap<Command, Boolean> = mutableMapOf()

var Command.permissions: List<Permission>
    get() {
        return permissionList[this] ?: emptyList()
    }
    set(perms) {
        permissionList[this] = perms
    }

var Command.ownerOnly: Boolean
    get() {
        return ownerOnlyList[this] ?: false
    }
    set(perms) {
        ownerOnlyList[this] = perms
    }

@Precondition
fun meetsPermissionRequirements() = precondition {
    val commandPermissions = it.container[it.commandStruct.commandName]?.permissions ?: return@precondition Pass
    if (it.author.toMember(it.guild!!)?.hasPermission(commandPermissions) == true) return@precondition Pass

    if (
        it.container[it.commandStruct.commandName]?.ownerOnly == true &&
        Rin.Owners.contains(it.author.idLong)
    ) return@precondition Pass

    return@precondition Fail("You do not have permission to use this command.")
}