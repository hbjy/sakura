package dev.itshayden.rinTohsaka.models

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable

object Members : IntIdTable() {
    val discordId = long("discordId")
    val points = integer("points").default(0)
    val level = integer("level").default(0)
    val guild = reference("guildId", Guilds)
}

class Member(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Member>(Members)

    var discordId by Members.discordId
    var points by Members.points
    var level by Members.level

    var guild by Guild referencedOn Members.guild
}