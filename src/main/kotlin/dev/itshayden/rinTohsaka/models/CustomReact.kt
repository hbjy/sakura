package dev.itshayden.rinTohsaka.models

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable

object CustomReacts : IntIdTable() {
    val trigger = text("trigger")
    val content = text("content")
    val guild = reference("guild", Guilds)
}

class CustomReact(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<CustomReact>(CustomReacts)

    var trigger by CustomReacts.trigger
    var content by CustomReacts.content
    var guild by Guild referencedOn CustomReacts.guild
}