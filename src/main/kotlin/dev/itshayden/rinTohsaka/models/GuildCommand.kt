package dev.itshayden.rinTohsaka.models

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable

object GuildCommands : IntIdTable() {
    val name = text("name")
    val response = text("response")
    val guild = reference("guild", Guilds)
}

class GuildCommand(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<GuildCommand>(GuildCommands)

    var name by GuildCommands.name
    var response by GuildCommands.response
    var guild by Guild referencedOn GuildCommands.guild
}