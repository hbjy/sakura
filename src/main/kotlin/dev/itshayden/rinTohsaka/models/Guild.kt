package dev.itshayden.rinTohsaka.models

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable

object Guilds : IntIdTable() {
    val discordId = long("discordId")
}

class Guild(id: EntityID<Int>): IntEntity(id) {
    companion object : IntEntityClass<Guild>(Guilds)

    var discordId by Guilds.discordId
}