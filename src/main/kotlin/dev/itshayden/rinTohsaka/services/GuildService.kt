package dev.itshayden.rinTohsaka.services

import dev.itshayden.rinTohsaka.models.*
import me.aberrantfox.kjdautils.api.annotation.Service
import org.jetbrains.exposed.sql.transactions.transaction
import net.dv8tion.jda.api.entities.Guild as GuildEntity

@Service
class GuildService {
    fun getOrCreateGuild(guild: GuildEntity): Guild {
        val dbGuild = transaction {
            Guild.find { Guilds.discordId eq guild.idLong }.firstOrNull()
        }

        if(dbGuild !== null) return dbGuild

        return transaction {
            Guild.new {
                discordId = guild.idLong
            }
        }
    }

    fun getGuildMembers(guild: GuildEntity): List<Member> {
        val dbGuild = getOrCreateGuild(guild)

        return transaction {
            transaction {
                Member.find {
                    (Members.guild eq dbGuild.id)
                }.toList()
            }
        }
    }

    fun getGuildReacts(guild: GuildEntity): List<CustomReact> {
        val dbGuild = getOrCreateGuild(guild)

        return transaction {
            transaction {
                CustomReact.find {
                    (CustomReacts.guild eq dbGuild.id)
                }.toList()
            }
        }
    }

    fun getGuildCommands(guild: GuildEntity): List<GuildCommand> {
        val dbGuild = getOrCreateGuild(guild)

        return transaction {
            transaction {
                GuildCommand.find {
                    (GuildCommands.guild eq dbGuild.id)
                }.toList()
            }
        }
    }

}