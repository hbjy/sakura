package dev.itshayden.rinTohsaka.services

import dev.itshayden.rinTohsaka.models.Member
import dev.itshayden.rinTohsaka.models.Members
import me.aberrantfox.kjdautils.api.annotation.Service
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.transactions.transaction
import net.dv8tion.jda.api.entities.Guild as GuildEntity
import net.dv8tion.jda.api.entities.Member as MemberEntity

@Service
class UserService(private val guildService: GuildService) {

    fun getOrCreateUser(member: MemberEntity, memberGuild: GuildEntity): Member {
        val dbGuild = guildService.getOrCreateGuild(memberGuild)
        val dbMember = transaction {
            Member.find {
                (Members.discordId eq member.idLong) and (Members.guild eq dbGuild.id)
            }.firstOrNull()
        }

        if(dbMember !== null) return dbMember

        return transaction {
            Member.new {
                discordId = member.idLong
                guild = dbGuild
            }
        }
    }

}