package dev.itshayden.rinTohsaka.commands

import dev.itshayden.rinTohsaka.preconditions.permissions
import me.aberrantfox.kjdautils.api.dsl.command.CommandSet
import me.aberrantfox.kjdautils.api.dsl.command.commands
import me.aberrantfox.kjdautils.api.dsl.embed
import me.aberrantfox.kjdautils.internal.arguments.JsStringArg
import me.aberrantfox.kjdautils.internal.arguments.MessageArg
import net.dv8tion.jda.api.Permission

@CommandSet("Utility")
fun utilityCommands() = commands {
    command("embed", "e") {
        permissions = listOf(Permission.MESSAGE_MANAGE)
        description = "Creates an embed in the current channel."

        execute(JsStringArg, JsStringArg) {
            it.respond {
                title = it.args.component1()
                description = it.args.component2()
            }
        }
    }

    command("editEmbed", "ee") {
        permissions = listOf(Permission.MESSAGE_MANAGE)
        description = "Edits an embed previously send."

        execute(MessageArg, JsStringArg, JsStringArg) {
            val message = it.args.component1()
            if (message.author.name != it.message.jda.selfUser.name) {
                it.respond("I didn't create that embed, so I can't change it! Sorry!")
            } else {
                if(message.embeds.count() == 0) {
                    it.respond("That message has no embed! Sorry!")
                } else {
                    val newEmbed = embed {
                        title = it.args.component2()
                        description = it.args.component3()
                    }

                    try {
                        message.editMessage(newEmbed)
                    } catch (e: Exception) {
                        it.respond("Sorry, I wasn't able to edit that!")
                    }
                }
            }
        }
    }
}