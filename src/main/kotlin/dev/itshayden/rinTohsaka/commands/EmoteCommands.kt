package dev.itshayden.rinTohsaka.commands

import me.aberrantfox.kjdautils.api.dsl.command.CommandSet
import me.aberrantfox.kjdautils.api.dsl.command.commands
import me.aberrantfox.kjdautils.api.dsl.embed
import me.aberrantfox.kjdautils.extensions.jda.toMember
import me.aberrantfox.kjdautils.internal.arguments.MemberArg

private val hugImages = arrayListOf(
    "http://gifimage.net/wp-content/uploads/2017/06/anime-hug-gif-12.gif",
    "https://cdn.weeb.sh/images/ryPix0Ft-.gif",
    "http://gifimage.net/wp-content/uploads/2017/01/Anime-hug-GIF-Image-Download-27.gif",
    "https://thumbs.gfycat.com/EthicalNecessaryAssassinbug-size_restricted.gif",
    "https://media.tenor.co/images/42922e87b3ec288b11f59ba7f3cc6393/raw",
    "https://gifimage.net/wp-content/uploads/2017/09/anime-girl-hug-gif-3.gif"
)

private val nuzzleImages = arrayListOf(
    "http://gifimage.net/wp-content/uploads/2017/09/anime-nuzzle-gif-5.gif",
    "http://i.imgur.com/V79eI.gif",
    "https://media.giphy.com/media/qngHjKb9ZZ2ms/giphy.gif",
    "http://gifimage.net/wp-content/uploads/2017/09/anime-nuzzle-gif-4.gif",
    "https://media1.tenor.com/images/a2b938d651a8f6b89ed4c02f9f8c13ed/tenor.gif?itemid=12010176",
    "http://media.tumblr.com/tumblr_lnk223ww5u1qafrh6.gif"
)

@CommandSet("Emote")
fun emoteCommands() = commands {
    command("hug") {
        description = "Hugs another user"

        execute(MemberArg) {
            val member = it.args.component1()
            val embed = embed {
                image = hugImages.shuffled().take(1)[0]
            }

            it.respond("💬 **${member.effectiveName}**, you've been hugged by **${it.author.toMember(it.guild!!)?.effectiveName}**! Nya~!")
            it.respond(embed)
        }
    }

    command("nuzzle") {
        description = "Nuzzles another user"

        execute(MemberArg) {
            val member = it.args.component1()
            val embed = embed {
                image = nuzzleImages.shuffled().take(1)[0]
            }

            it.respond("💬 **${member.effectiveName}**, you've been nuzzled by **${it.author.toMember(it.guild!!)?.effectiveName}**! UwU~!")
            it.respond(embed)
        }
    }
}