package dev.itshayden.rinTohsaka.commands

import dev.itshayden.rinTohsaka.models.CustomReact
import dev.itshayden.rinTohsaka.models.CustomReacts
import dev.itshayden.rinTohsaka.preconditions.permissions
import dev.itshayden.rinTohsaka.services.GuildService
import me.aberrantfox.kjdautils.api.dsl.command.CommandSet
import me.aberrantfox.kjdautils.api.dsl.command.commands
import me.aberrantfox.kjdautils.api.dsl.embed
import me.aberrantfox.kjdautils.internal.arguments.IntegerArg
import me.aberrantfox.kjdautils.internal.arguments.JsStringArg
import net.dv8tion.jda.api.Permission
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.transactions.transaction

@CommandSet()
fun reactCommands(guildService: GuildService) = commands {
    command("addreact", "ar") {
        permissions = listOf(Permission.MESSAGE_MANAGE)
        description = "Creates a custom reaction for the server."

        execute(JsStringArg, JsStringArg) {
            val (trig, cont) = it.args
            val dbGuild = guildService.getOrCreateGuild(it.guild!!)

            val react = transaction {
                CustomReact.new {
                    trigger = trig
                    content = cont
                    guild = dbGuild
                }
            }

            it.respond("Reaction ${react.trigger} created, with content ${react.content}.")
        }
    }

    command("listreact", "lr") {
        description = "Lists the custom reactions available on the server."

        execute() {
            val names = guildService.getGuildReacts(it.guild!!).map {
                "`it.trigger` (${it.id})"
            }

            it.respond(embed {
                title = "Reacts for ${it.guild!!.name}"
                description = names.joinToString {
                    "$it,"
                }
            })
        }
    }

    command("deletereact", "dr") {
        permissions = listOf(Permission.MESSAGE_MANAGE)
        description = "Deletes a custom reaction for the server."

        execute(IntegerArg) {
            val react = guildService.getGuildReacts(it.guild!!)
                .find { r ->
                    r.id.value == it.args.component1()
                    r.guild.discordId == it.guild!!.idLong
                }

            if(react == null) {
                it.respond("That react doesn't exist!")
            } else {
                transaction {
                    CustomReacts.deleteWhere {
                        CustomReacts.id eq it.args.component1()
                    }
                }
                it.respond("Done!")
            }

        }
    }
}