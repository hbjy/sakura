package dev.itshayden.rinTohsaka.commands

import me.aberrantfox.kjdautils.api.dsl.command.CommandSet
import me.aberrantfox.kjdautils.api.dsl.command.commands
import me.aberrantfox.kjdautils.internal.arguments.IntegerArg
import me.aberrantfox.kjdautils.internal.arguments.WordArg
import net.dv8tion.jda.api.entities.TextChannel

@CommandSet("Fun")
fun funCommands() = commands {

    command("xkcd") {
        description = "Gets an XKCD comic."

        execute(IntegerArg) {
            val comic = it.args.component1()
            val r = khttp.get(
                "https://xkcd.com/${comic}/info.0.json"
            )

            if(r.statusCode == 404)
                it.respond("No such comic found!")
            else
                it.respond {
                    title = "xkcd #${r.jsonObject.getInt("num")}: ${r.jsonObject.getString("safe_title")}"
                    image = r.jsonObject.getString("img")
                    description = r.jsonObject.getString("alt")
                }
        }
    }

    command("joke", "j") {
        description = "Gets a random joke."

        execute(WordArg.makeOptional("miscellaneous")) {
            val type = it.args.component1().toLowerCase()
            val j = khttp.get(
                "https://sv443.net/jokeapi/category/$type${if (!(it.channel as TextChannel).isNSFW()) "?blacklist=nsfw" else ""}"
            ).jsonObject
            if (j.getString("type") == "twopart") {
                it.respond("${j.getString("setup")}\n${j.getString("delivery")}")
            } else {
                it.respond(j.getString("joke"))
            }
        }
    }

    command("dadjoke", "dj") {
        description = "Gets a random dad joke."

        execute {
            val j = khttp.get("https://icanhazdadjoke.com", headers = mapOf("Accept" to "text/plain")).text
            it.respond(j)
        }
    }

}