package dev.itshayden.rinTohsaka.commands

import dev.itshayden.rinTohsaka.arguments.AnimeArg
import dev.itshayden.rinTohsaka.arguments.MangaArg
import me.aberrantfox.kjdautils.api.dsl.command.CommandSet
import me.aberrantfox.kjdautils.api.dsl.command.commands

@CommandSet("Weeb Stuff")
fun weebCommands() = commands {
    command("anime", "a") {
        description = "Gets anime information from MyAnimeList!"

        execute(AnimeArg) {
            val anime = it.args.component1()

            it.respond {
                author {
                    name = "${anime.getString("title")} **(${anime.getString("rated")})**"
                    url = anime.getString("url")
                }

                description = anime.getString("synopsis")

                field {
                    name = "Still airing?"
                    value = when(anime.getBoolean("airing")) {
                        true -> "Yes"
                        false -> "No"
                    }
                    inline = true
                }

                field {
                    name = "Type"
                    value = anime.getString("type")
                    inline = true
                }

                field {
                    name = "Episodes"
                    value = anime.getInt("episodes").toString()
                    inline = true
                }

                field {
                    name = "MyAnimeList Score"
                    value = anime.getInt("score").toString()
                    inline = true
                }

                thumbnail = anime.getString("image_url")

                footer {
                    text = "Information from MyAnimeList via Jikan API."
                }
            }
        }
    }

    command("manga", "m") {
        description = "Gets manga information from MyAnimeList!"

        execute(MangaArg) {
            val manga = it.args.component1()

            it.respond {
                author {
                    name = manga.getString("title")
                    url = manga.getString("url")
                }

                description = manga.getString("synopsis")

                field {
                    name = "Still publishing?"
                    value = when(manga.getBoolean("publishing")) {
                        true -> "Yes"
                        false -> "No"
                    }
                    inline = true
                }

                field {
                    name = "Volumes"
                    value = manga.getInt("volumes").toString()
                    inline = true
                }

                field {
                    name = "Chapters"
                    value = manga.getInt("chapters").toString()
                    inline = true
                }

                field {
                    name = "MyAnimeList Score"
                    value = manga.getInt("score").toString()
                    inline = true
                }

                thumbnail = manga.getString("image_url")

                footer {
                    text = "Information from MyAnimeList via Jikan API."
                }
            }
        }
    }
}