package dev.itshayden.rinTohsaka.commands

import dev.itshayden.rinTohsaka.models.GuildCommand
import dev.itshayden.rinTohsaka.models.GuildCommands
import dev.itshayden.rinTohsaka.preconditions.permissions
import dev.itshayden.rinTohsaka.services.GuildService
import me.aberrantfox.kjdautils.api.dsl.command.CommandSet
import me.aberrantfox.kjdautils.api.dsl.command.commands
import me.aberrantfox.kjdautils.api.dsl.embed
import me.aberrantfox.kjdautils.internal.arguments.IntegerArg
import me.aberrantfox.kjdautils.internal.arguments.JsStringArg
import me.aberrantfox.kjdautils.internal.arguments.WordArg
import net.dv8tion.jda.api.Permission
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.transactions.transaction

@CommandSet("Guild Customs")
fun guildCommandCommands(guildService: GuildService) = commands {
    command("addcustom", "ac") {
        permissions = listOf(Permission.MESSAGE_MANAGE)
        description = "Creates a custom command for the server."

        execute(WordArg, JsStringArg) {
            val (trig, cont) = it.args
            val dbGuild = guildService.getOrCreateGuild(it.guild!!)

            try {
                val custom = transaction {
                    GuildCommand.new {
                        name = trig.toLowerCase()
                        response = cont
                        guild = dbGuild
                    }
                }
                it.respond("Reaction ${custom.name} created, with response ${custom.response}.")
            } catch (e: Exception) {
                it.respond("Something went wrong! Perhaps there is already a command with that name?")
            }
        }
    }

    command("listcustoms", "lc") {
        description = "Lists the custom commands available on the server."

        execute() {
            val names = guildService.getGuildCommands(it.guild!!).map {
                "`${it.name}` (${it.id})"
            }

            it.respond(embed {
                title = "Commands for ${it.guild!!.name}"
                description = names.joinToString {
                    "$it,"
                }
            })
        }
    }

    command("deletecustom", "dc") {
        permissions = listOf(Permission.MESSAGE_MANAGE)
        description = "Deletes a custom command for the server."

        execute(IntegerArg) {
            val react = guildService.getGuildReacts(it.guild!!)
                .find { r ->
                    r.id.value == it.args.component1()
                }

            if(react == null) {
                it.respond("That command doesn't exist!")
            } else {
                transaction {
                    GuildCommands.deleteWhere {
                        GuildCommands.id eq it.args.component1()
                    }
                }
                it.respond("Done!")
            }

        }
    }
}