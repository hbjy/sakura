package dev.itshayden.rinTohsaka.commands

import dev.itshayden.rinTohsaka.models.Members
import dev.itshayden.rinTohsaka.preconditions.permissions
import dev.itshayden.rinTohsaka.services.GuildService
import dev.itshayden.rinTohsaka.services.UserService
import me.aberrantfox.kjdautils.api.dsl.command.CommandSet
import me.aberrantfox.kjdautils.api.dsl.command.commands
import me.aberrantfox.kjdautils.extensions.jda.toMember
import me.aberrantfox.kjdautils.internal.arguments.MemberArg
import net.dv8tion.jda.api.Permission
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.transactions.transaction

@CommandSet("Economy")
fun economyCommands(userService: UserService, guildService: GuildService) = commands {
    command("points", "p") {
        description = "Shows you how many points you (or another member) have!"

        execute(MemberArg.makeOptional { it.author.toMember(it.guild!!)!! }) {
            val member = it.args.component1()

            val dbMember = userService.getOrCreateUser(member, it.guild!!)

            it.respond {
                title = "Points for ${member.effectiveName}"
                description =
                    "**${member.effectiveName}** has **${dbMember.points} points**.\n They are currently **level ${dbMember.level}**."
                thumbnail = member.user.effectiveAvatarUrl
            }
        }
    }

    command("leaderboard", "lb") {
        description = "Shows you the top 5 users on your server!"

        execute {
            val guild = it.guild!!
            val membersList = guildService.getGuildMembers(guild).sortedByDescending { m -> m.points }
            val members = membersList.takeWhile { m -> membersList.indexOf(m) <= 5 }
            var levels = ""
            members.forEach { member ->
                val mem = it.guild?.getMemberById(member.discordId)
                levels += "⭐ **${mem?.effectiveName}** - Level ${member.level} (${member.points} points)\n"
            }

            it.respond {
                title = "Leaderboard for ${guild.name}"
                description = levels.trim()
            }
        }
    }

    command("resetScores", "rs") {
        description = "Resets the scores for the current guild."
        permissions = listOf(Permission.ADMINISTRATOR)

        execute {
            val guild = guildService.getOrCreateGuild(it.guild!!)

            transaction {
                Members.deleteWhere { Members.guild eq guild.id }
            }

            it.respond {
                title = "Reset scores for the server!"
            }
        }
    }
}