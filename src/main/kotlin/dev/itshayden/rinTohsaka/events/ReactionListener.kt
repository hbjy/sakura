package dev.itshayden.rinTohsaka.events

import com.google.common.eventbus.Subscribe
import dev.itshayden.rinTohsaka.services.GuildService
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent

class ReactionListener(private val guildService: GuildService) {

    @Subscribe
    fun onMessage(event: GuildMessageReceivedEvent) {
        if(event.author.isBot) return
        val reacts = guildService.getGuildReacts(event.guild)

        for (react in reacts.shuffled()) {
            val isReact = event.message.contentStripped.toLowerCase().contains(react.trigger.toLowerCase())
            if(!isReact) return

            event.channel.sendMessage(react.content).queue()
        }
    }

}