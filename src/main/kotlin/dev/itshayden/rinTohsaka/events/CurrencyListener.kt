package dev.itshayden.rinTohsaka.events

import com.google.common.eventbus.Subscribe
import dev.itshayden.rinTohsaka.services.UserService
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent
import org.jetbrains.exposed.sql.transactions.transaction
import kotlin.math.floor
import kotlin.math.sqrt

class CurrencyListener(private val userService: UserService) {
    @Subscribe
    fun onMessage(event: GuildMessageReceivedEvent) {
        if(event.author.isBot) return
        val author = event.guild.getMember(event.author)!!
        val member = this.userService.getOrCreateUser(event.member!!, event.guild)

        transaction {
            member.points = member.points.plus(1)
        }

        val level = floor(0.4 * sqrt(member.points.toDouble())).toInt()
        if(level <= member.level) return

        transaction {
            member.level = level
        }

        event.channel.sendMessage(
            "Well done, **${author.effectiveName}**, you levelled up to **level ${level}**!"
        ).queue()
    }
}