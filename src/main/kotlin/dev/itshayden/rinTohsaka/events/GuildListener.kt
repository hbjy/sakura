package dev.itshayden.rinTohsaka.events

import com.google.common.eventbus.Subscribe
import dev.itshayden.rinTohsaka.services.GuildService
import net.dv8tion.jda.api.events.guild.GuildJoinEvent

class GuildListener(private val guildService: GuildService) {

    @Subscribe
    fun onGuildJoin(event: GuildJoinEvent) {
        guildService.getOrCreateGuild(event.guild)
    }

}