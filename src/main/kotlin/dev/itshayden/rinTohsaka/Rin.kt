package dev.itshayden.rinTohsaka

import me.aberrantfox.kjdautils.api.dsl.PrefixDeleteMode
import me.aberrantfox.kjdautils.api.startBot

object Rin {
    val Owners = listOf(508429235157729290)

    private val token = System.getenv("DISCORD_TOKEN")
        ?: throw Error("You must provide a Discord token using the DISCORD_TOKEN environment variable.")

    fun start() {
        startBot(token) {
            configure {
                prefix = ")"
                reactToCommands = true
                deleteMode = PrefixDeleteMode.Double
            }
        }
    }
}