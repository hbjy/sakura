package dev.itshayden.rinTohsaka.arguments

import khttp.responses.Response
import me.aberrantfox.kjdautils.api.dsl.command.CommandEvent
import me.aberrantfox.kjdautils.internal.command.ArgumentResult
import me.aberrantfox.kjdautils.internal.command.ArgumentType
import me.aberrantfox.kjdautils.internal.command.ConsumptionType
import org.json.JSONObject

open class AnimeArg(override val name: String = "Anime") : ArgumentType<JSONObject>() {
    companion object : AnimeArg()

    override val consumptionType = ConsumptionType.All
    override val examples = arrayListOf("Fate/Zero", "Sword Art Online", "Violet Evergarden")

    override fun convert(arg: String, args: List<String>, event: CommandEvent<*>): ArgumentResult<JSONObject> {
        val result = searchJikan("anime", args.joinToString(" "))

        return if (result != null)
            ArgumentResult.Success(result, args)
        else
            ArgumentResult.Error("No such anime found.")

    }
}

open class MangaArg(override val name: String = "Manga") : ArgumentType<JSONObject>() {
    companion object : MangaArg()

    override val consumptionType = ConsumptionType.All
    override val examples = arrayListOf("Fate/Zero", "Sword Art Online", "Violet Evergarden")

    override fun convert(arg: String, args: List<String>, event: CommandEvent<*>): ArgumentResult<JSONObject> {
        val result = searchJikan("manga", args.joinToString(" "))

        return if (result != null)
            ArgumentResult.Success(result, args)
        else
            ArgumentResult.Error("No such manga found.")

    }
}

private fun searchJikan(type: String, search: String): JSONObject? {
    val r: Response = khttp.get("https://api.jikan.moe/v3/search/${type}?q=${search}")
    val results = r.jsonObject.getJSONArray("results")

    return if (results.length() != 0)
        results.getJSONObject(0)
    else
        null
}