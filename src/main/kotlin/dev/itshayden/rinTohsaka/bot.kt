package dev.itshayden.rinTohsaka

import dev.itshayden.rinTohsaka.models.CustomReacts
import dev.itshayden.rinTohsaka.models.GuildCommands
import dev.itshayden.rinTohsaka.models.Guilds
import dev.itshayden.rinTohsaka.models.Members
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction

fun main() {
    val uri = System.getenv("DB_URI") ?: throw Error("You must include a valid PostgreSQL URI in the DB_URI variable.")
    Database.connect(uri, driver = "org.postgresql.Driver")

    transaction {
        SchemaUtils.createMissingTablesAndColumns(Guilds, Members, CustomReacts, GuildCommands)
    }

    Rin.start()
}