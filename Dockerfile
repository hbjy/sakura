FROM gradle AS build
WORKDIR /build
COPY src ./src
RUN ls
COPY gradle.properties .
COPY settings.gradle .
COPY build.gradle .
RUN gradle shadowJar

FROM build AS rin
WORKDIR /app
COPY --from=build /build/build/libs/*-all.jar ./bot.jar
CMD ["java", "-jar", "/app/bot.jar"]