# Commands

## Key
| Symbol     | Meaning                    |
| ---------- | -------------------------- |
| (Argument) | This argument is optional. |

## Economy
| Commands        | Arguments | Description                                             |
| --------------- | --------- | ------------------------------------------------------- |
| leaderboard, lb | <none>    | Shows you the top 5 users on your server!               |
| points, p       | (Member)  | Shows you how many points you (or another member) have! |
| resetScores, rs | <none>    | Resets the scores for the current guild.                |

## Emote
| Commands | Arguments | Description          |
| -------- | --------- | -------------------- |
| hug      | Member    | Hugs another user    |
| nuzzle   | Member    | Nuzzles another user |

## Fun
| Commands    | Arguments | Description             |
| ----------- | --------- | ----------------------- |
| dadjoke, dj | <none>    | Gets a random dad joke. |
| joke, j     | (Word)    | Gets a random joke.     |
| xkcd        | Integer   | Gets an XKCD comic.     |

## Guild Customs
| Commands         | Arguments         | Description                                        |
| ---------------- | ----------------- | -------------------------------------------------- |
| addcustom, ac    | Word, JsStringArg | Creates a custom command for the server.           |
| deletecustom, dc | Integer           | Deletes a custom command for the server.           |
| listcustoms, lc  | <none>            | Lists the custom commands available on the server. |

## Utility
| Commands      | Arguments                           | Description                              |
| ------------- | ----------------------------------- | ---------------------------------------- |
| Help          | (Command)                           | Display a help menu.                     |
| editEmbed, ee | MessageID, JsStringArg, JsStringArg | Edits an embed previously send.          |
| embed, e      | JsStringArg, JsStringArg            | Creates an embed in the current channel. |

## Weeb Stuff
| Commands | Arguments | Description                              |
| -------- | --------- | ---------------------------------------- |
| anime, a | Anime     | Gets anime information from MyAnimeList! |
| manga, m | Manga     | Gets manga information from MyAnimeList! |

## uncategorized
| Commands        | Arguments                | Description                                         |
| --------------- | ------------------------ | --------------------------------------------------- |
| addreact, ar    | JsStringArg, JsStringArg | Creates a custom reaction for the server.           |
| deletereact, dr | Integer                  | Deletes a custom reaction for the server.           |
| listreact, lr   | <none>                   | Lists the custom reactions available on the server. |

